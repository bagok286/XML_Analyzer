package com.xml.analyzer.app;

import static java.util.Arrays.asList;

import com.google.inject.Inject;
import com.xml.analyzer.models.Request;
import com.xml.analyzer.service.api.IRequestProcessor;
import com.xml.analyzer.service.api.ISearcher;
import com.xml.analyzer.service.api.IResponseConstructor;
import com.xml.analyzer.service.api.IXmlReader;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;

import javafx.util.Pair;

/**
 * @@author Vladyslav_Vinnyk on 7/1/2018
 */
public class XmlAnalyzerApp {

    @Inject
    private IRequestProcessor<List<String>, Request> requestProcessor;

    @Inject
    private IXmlReader<Document, String> reader;

    @Inject
    private IResponseConstructor<Pair<Integer, Element>> responseConstructor;

    @Inject
    private ISearcher<Element, Document, Pair<Integer, Element>> similarElementSearcher;

    public void process(String[] args) {
        Request request = requestProcessor.process(asList(args));
        Document originalDocument = reader.read(request.getInputOriginFilePath());
        Document otherDocument = reader.read(request.getInputOtherSampleFilePath());

        Pair<Integer, Element> similarElement = similarElementSearcher.search(findSourceElementToFindSimilarityAgainst(request, originalDocument), otherDocument);

        responseConstructor.sendResponse(similarElement);
    }

    private Element findSourceElementToFindSimilarityAgainst(Request request, Document originalDocument) {
        return originalDocument.getElementById(request.getElementId());
    }
}