package com.xml.analyzer.app;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.xml.analyzer.models.Request;
import com.xml.analyzer.service.api.IRequestProcessor;
import com.xml.analyzer.service.api.ISearcher;
import com.xml.analyzer.service.api.IResponseConstructor;
import com.xml.analyzer.service.api.IXmlReader;
import com.xml.analyzer.service.impl.HtmlReader;
import com.xml.analyzer.service.impl.HtmlSearcher;
import com.xml.analyzer.service.impl.IConsoleOutPutView;
import com.xml.analyzer.service.impl.RequestConstructor;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;

import javafx.util.Pair;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public class BasicModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(new TypeLiteral<IRequestProcessor<List<String>, Request>>(){}).to(RequestConstructor.class);
        bind(new TypeLiteral<IXmlReader<Document, String>>(){}).to(HtmlReader.class);
        bind(new TypeLiteral<IResponseConstructor<Pair<Integer, Element>>>(){}).to(IConsoleOutPutView.class);
        bind(new TypeLiteral<ISearcher<Element, Document, Pair<Integer, Element>>>(){}).to(HtmlSearcher.class);
    }
}
