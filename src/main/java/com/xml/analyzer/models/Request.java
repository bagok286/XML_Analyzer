package com.xml.analyzer.models;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
@Builder
@Data
public class Request {
    @NonNull
    private String inputOriginFilePath;
    @NonNull
    private String inputOtherSampleFilePath;
    private String elementId;

}
