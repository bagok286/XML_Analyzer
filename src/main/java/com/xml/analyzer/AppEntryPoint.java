package com.xml.analyzer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.xml.analyzer.app.BasicModule;
import com.xml.analyzer.app.XmlAnalyzerApp;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public class AppEntryPoint  {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new BasicModule());
        XmlAnalyzerApp xmlAnalyzerApp = injector.getInstance(XmlAnalyzerApp.class);

        xmlAnalyzerApp.process(args);
    }

}