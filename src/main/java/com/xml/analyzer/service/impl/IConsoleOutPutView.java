package com.xml.analyzer.service.impl;

import static java.lang.System.lineSeparator;
import static java.lang.System.out;

import com.google.inject.Singleton;
import com.xml.analyzer.service.api.IResponseConstructor;

import org.jsoup.nodes.Element;

import javafx.util.Pair;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
@Singleton
public class IConsoleOutPutView implements IResponseConstructor<Pair<Integer, Element>> {

    @Override
    public void sendResponse(Pair<Integer, Element> source) {
        out.println("Attribute similarity: " + source.getKey() + lineSeparator()
            + "Element: " + source.getValue());
    }
}
