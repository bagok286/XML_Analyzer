package com.xml.analyzer.service.impl;

import static com.google.common.base.Objects.firstNonNull;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;

import com.google.inject.Singleton;
import com.xml.analyzer.models.Request;
import com.xml.analyzer.service.api.IRequestProcessor;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;


/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
@Singleton
public class RequestConstructor implements IRequestProcessor<List<String>, Request> {

    private static final String INPUT_ORIGIN_FILE_PATH_MUST_BE_PRESENT = "inputOriginFilePath is required param";
    private static final String INPUT_OTHER_FILE_PATH_MUST_BE_PRESENT = "inputOtherSampleFilePath is required param";
    private static final String DEFAULT_ELEMENT_ID = "make-everything-ok-button";

    @Override
    public Request process(List<String> args) {
        Map<Integer, String> arguments = IntStream.range(0, args.size())
            .boxed()
            .collect(toMap(i -> i, args::get));

        return Request.builder()
            .inputOriginFilePath(ofNullable(arguments.get(0)).orElseThrow(() ->
                new IllegalArgumentException(INPUT_ORIGIN_FILE_PATH_MUST_BE_PRESENT)))
            .inputOtherSampleFilePath(ofNullable(arguments.get(1)).orElseThrow(() ->
                new IllegalArgumentException(INPUT_OTHER_FILE_PATH_MUST_BE_PRESENT)))
            .elementId(firstNonNull(arguments.get(2), DEFAULT_ELEMENT_ID))
            .build();
    }
}
