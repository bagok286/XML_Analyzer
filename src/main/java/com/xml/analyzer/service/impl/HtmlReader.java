package com.xml.analyzer.service.impl;

import static java.nio.charset.StandardCharsets.*;

import com.google.inject.Singleton;
import com.xml.analyzer.models.exception.BusinessException;
import com.xml.analyzer.service.api.IXmlReader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
@Singleton
public class HtmlReader implements IXmlReader<Document, String> {
    private static final String pathToResources = "src/main/resources/";

    @Override
    public Document read(String path) {
        return parseIntern(path);
    }

    private Document parseIntern(String path) {
        try {
            return Jsoup.parse(new File(pathToResources + path), UTF_8.name());
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }
}
