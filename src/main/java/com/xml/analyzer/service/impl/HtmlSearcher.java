package com.xml.analyzer.service.impl;

import static org.apache.commons.collections4.CollectionUtils.intersection;

import com.google.inject.Singleton;
import com.xml.analyzer.service.api.ISearcher;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Comparator;

import javafx.util.Pair;


/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
@Singleton
public class HtmlSearcher implements ISearcher<Element, Document, Pair<Integer, Element>> {
    private Pair<Integer, Element> emptyPair = new Pair<>(0, null);

    @Override
    public Pair<Integer, Element> search(Element sourceElement, Document docToFindSimilarElement) {
        Attributes attributes = sourceElement.attributes();
        String sourceElementName = sourceElement.tagName();

        return docToFindSimilarElement.getAllElements().stream()
            .filter(item -> StringUtils.equals(sourceElementName, item.tagName()))
            .map(element -> new Pair<>(intersection(element.attributes(), attributes).size(), element))
            .max(Comparator.comparingInt(Pair::getKey)).orElse(emptyPair);
    }
}