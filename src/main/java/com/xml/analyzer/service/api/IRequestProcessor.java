package com.xml.analyzer.service.api;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public interface IRequestProcessor<S, R> {
    R process(S source);
}
