package com.xml.analyzer.service.api;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public interface ISearcher<S, O,  R> {
    R search(S source, O other);
}
