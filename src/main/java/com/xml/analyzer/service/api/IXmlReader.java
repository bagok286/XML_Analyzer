package com.xml.analyzer.service.api;

import java.io.IOException;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public interface IXmlReader<R, S> {
    R read(S source);
}
