package com.xml.analyzer.service.api;

/**
 * @author Vladyslav_Vinnyk on 7/1/2018
 */
public interface IResponseConstructor<S> {
    void sendResponse(S source);
}
