<b>To build:</b> ./gradlew clean build


<b>To run:</b>
   
    a) <u>java -cp XML_Analyzer-1.0-SNAPSHOT.jar com.xml.analyzer.AppEntryPoint startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-1-evil-gemini.html</u>

    b) <u>java -cp XML_Analyzer-1.0-SNAPSHOT.jar com.xml.analyzer.AppEntryPoint startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-2-container-and-clone.html</u>

    c) <u>java -cp XML_Analyzer-1.0-SNAPSHOT.jar com.xml.analyzer.AppEntryPoint startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-3-the-escape.html</u>

    d) <u>java -cp XML_Analyzer-1.0-SNAPSHOT.jar com.xml.analyzer.AppEntryPoint startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-4-the-mash.html</u>


<b>NOTE: That you can pass element id as third paramemter e.g:</b>

     <u>java -cp XML_Analyzer-1.0-SNAPSHOT.jar com.xml.analyzer.AppEntryPoint startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-1-evil-gemini.html side-menu</u>